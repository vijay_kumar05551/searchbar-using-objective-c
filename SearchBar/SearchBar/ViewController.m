//
//  ViewController.m
//  SearchBar
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating>
{
    NSArray *records;
    UISearchController *searchController;
    NSArray *searchFilterArray;
}
@end

@implementation ViewController
@synthesize showRecordsTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    records = [[NSArray alloc] initWithObjects:@"One", @"Two", @"Three", @"Four", @"Five", nil];
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.delegate = self;
    searchController.searchBar.delegate = self;
    searchController.searchResultsUpdater = self;
    searchController.dimsBackgroundDuringPresentation = NO;
    [searchController.searchBar sizeToFit];
    showRecordsTableView.tableHeaderView = searchController.searchBar;
    
    searchFilterArray = records;
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    if (searchController.searchBar.text.length > 0)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",searchController.searchBar.text];
        
        records = [records filteredArrayUsingPredicate:predicate];
        [showRecordsTableView reloadData];
    }
    else {
        [showRecordsTableView reloadData];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    records = searchFilterArray;
    [showRecordsTableView reloadData];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"recordsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [records objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"%ld",(long)indexPath.row);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
